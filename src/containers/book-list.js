import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { selectBook } from '../actions/index';

class BookList extends Component {
    _renderList() {
        return this
            .props
            .books
            .map(book => {
                return (
                    <li key={book.title} onClick={this.props.selectBook(book)} className='list-group-item'>{book.title}</li>
                )
            });
    }

    render() {
        return (
            <ul className='col-sm-4 list-group'>
                {this._renderList()}
            </ul>
        )
    }
}

function mapStateToProps(state) {
    return {books: state.books}
}

function dispatchMapToprops(dispatch){
    return bindActionCreators({selectBook:selectBook},dispatch);
}

export default connect(mapStateToProps)(BookList);